# vue

#### 介绍
由于市面上没有较完整的vue-sku后台组件，于是我在vue-sku-form的基础上优化了一下
已经独立出来，只需要一个单组件引入即可使用，相对简便

#### 安装教程

直接克隆下载，把包解压到您的vue项目即可

#### 依赖

该插件需要用到element-ui库的el-form组件

#### 使用说明

由于插件的图片上传需要用到图片库组件，可能无法使用
请自行修改或替换为自己的图片库组件

1.引入插件和参数配置
~~~vue
<script>
import SkuForm from '@/components/chenmsku'
export default {
      data(){
        return {
            //sku默认规格组
            sourceAttribute: [{
                "name": "版本",
                "value": null,
                "item": [{
                    label: '6+128',
                    value: null,

                }, {
                    label: '8+128',
                    value: null,
                }]
            }, {
                "name": "套装",
                "value": null,
                "item": [{
                    label: '标配',
                    value: null,

                }, {
                    label: '快充套装',
                    value: null,
                }]
            }],
            //structure
            structure: [], 
            //商品数据
            form: {
                //sku数据
                skuJson: {
                    //sku规格属性
                    "attribute": [{
                        "name": "测试",
                        "value": null,
                        "item": [{
                            label: '测试6+128',
                            value: null,

                        }, {
                            label: '测试8+128',
                            value: null,
                        }]
                    }, {
                        "name": "测试2",
                        "value": null,
                        "item": [{
                            label: '测试标配',
                            value: null,

                        }, {
                            label: '测试快充套装',
                            value: null,
                        }]
                    }],
                    //sku集
                    "sku": []
                }
            },
        }
     }
     components: {
        SkuForm
    },
}
</script>
~~~
2.视图的使用
~~~vue
<template>
<div>
    <div>
        <el-input v-model="attrName" placeholder="添加一个属性">
            <el-button slot="append" @click="attributeAdd">添加</el-button>
        </el-input>
        <div style="display: flex;">
            <small class="el-form-tips">注意：当属性规格超过48种组合，在操作属性规格时可能会稍有卡顿，属于正常现象！</small>
        </div>
    </div>
    <SkuForm ref="skuForm" :structure.sync="structure" :source-attribute="sourceAttribute" :attribute-num-max="5" :attribute.sync="form.skuJson.attribute" @update:attribute="(data)=>{form.skuJson.attribute = data}" @update:sku="(data) =>{ onUpdateSku(data)}" :sku.sync="form.skuJson.sku">
        <template #image="slotProps">
            <div class="el-upload-xs-sm el-upload-view">
                <div v-if="typeof slotProps.row.image=='object' && slotProps.row.image.url" class="upload-list upload-list-center">
                    <div class="upload-list-item">
                        <div class="el-upload-img-cover" @click="preViewSku(slotProps.row.image.url)" :style="{'background-image':'url('+slotProps.row.image.url+')'}">
                            <span class="el-upload-img-delete" @click="delImageBySku(slotProps)">
                                <i class="el-icon-error"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div v-if="typeof slotProps.row.image!='object' || !slotProps.row.image.url" tabindex="0" class="el-upload el-upload--picture-card" @click="uploadImageBySku(slotProps)">
                    <i class="el-icon-plus"></i>
                    <input type="file" name="file" class="el-upload__input">
                </div>
            </div>
        </template>
    </SkuForm>
</div>
</template>
~~~
3.引入样式
~~~vue
<style scoped>
@import "@/assets/css/sku.css";
</style>
~~~